﻿using Microsoft.EntityFrameworkCore;
using NetCoreAPIBoilerplate.Models;

namespace NetCoreAPIBoilerplate.Context
{
    public class CabinContext : DbContext
    {
        public DbSet<Cabin> Cabins { get; set; } = default!;
        public DbSet<Discount> Discounts { get; set; } = default!;
        public DbSet<Reservation> Reservations { get; set; } = default!;
        public DbSet<User> Users { get; set; } = default!;


        public CabinContext(DbContextOptions<CabinContext> options) : base(options) { }

        /*
         * When the database is created, EF creates tables that have names the same as the DbSet property names. 
         * Property names for collections are typically plural. 
         */

        /*
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cabin>().ToTable("Cabin");
        }
        */
    }
}
