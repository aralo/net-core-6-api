﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace NetCoreAPIBoilerplate.Migrations
{
    public partial class NewModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "name",
                table: "Cabins",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "imageUrl",
                table: "Cabins",
                newName: "ImageUrl");

            migrationBuilder.RenameColumn(
                name: "description",
                table: "Cabins",
                newName: "Description");

            migrationBuilder.RenameColumn(
                name: "id",
                table: "Cabins",
                newName: "Id");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Cabins",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.CreateTable(
                name: "Discounts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    PercentAmount = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Discounts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Reservations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StartDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    PricePerDay = table.Column<float>(type: "real", nullable: false),
                    discountId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reservations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Reservations_Discounts_discountId",
                        column: x => x.discountId,
                        principalTable: "Discounts",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Reservations_discountId",
                table: "Reservations",
                column: "discountId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Reservations");

            migrationBuilder.DropTable(
                name: "Discounts");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Cabins",
                newName: "name");

            migrationBuilder.RenameColumn(
                name: "ImageUrl",
                table: "Cabins",
                newName: "imageUrl");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "Cabins",
                newName: "description");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Cabins",
                newName: "id");

            migrationBuilder.AlterColumn<string>(
                name: "description",
                table: "Cabins",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255);
        }
    }
}
