﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NetCoreAPIBoilerplate.Models
{
    public class Reservation
    {
        [Key]
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public float PricePerDay { get; set; }
        [NotMapped]
        public double TotalReservationAmount { get => (EndDate - StartDate).TotalDays * PricePerDay; }
        public Discount? discount { get; set; }
    }
}
