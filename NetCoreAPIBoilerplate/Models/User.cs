﻿using System.ComponentModel.DataAnnotations;

namespace NetCoreAPIBoilerplate.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Missing username.")]
        public string Username { get; set; } = string.Empty;

        [Required(ErrorMessage = "Missing password.")]
        public string Password { get; set; } = string.Empty;

        public int RoleLevel { get; set; } = 0;

    }
}
