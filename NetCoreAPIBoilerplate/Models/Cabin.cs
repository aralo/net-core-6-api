﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace NetCoreAPIBoilerplate.Models
{
    public class Cabin
    {
        [JsonIgnore]
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(255)] 
        public string Name { get; set; } = string.Empty!;
        public string ImageUrl { get; set; } = string.Empty;
        [MinLength(5, ErrorMessage = "Description should be at last 5 characters."), MaxLength(255)]
        public string Description { get; set; } = string.Empty;
    }
}
