﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NetCoreAPIBoilerplate.Models
{
    [NotMapped]
    public class MonthlyReservation
    {
        public string MonthName { get; set; } = string.Empty;
        public List<Reservation> Reservations { get; set; } = new List<Reservation>();
        public double TotalRevenue { get; set; } = 0.0;
    }
}
