using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using NetCoreAPIBoilerplate.Auth;
using NetCoreAPIBoilerplate.Context;
using NetCoreAPIBoilerplate.Data;
using NetCoreAPIBoilerplate.Models;
using NetCoreAPIBoilerplate.Services;
using System.Text;

const string MyAllowSpecificOrigins = "CorsApi";
var builder = WebApplication.CreateBuilder(args);
var tokenKey = Encoding.ASCII.GetBytes(builder.Configuration.GetSection("TokenSecretKey").Value);

builder.Services.AddCors(options =>
{
    options.AddPolicy(MyAllowSpecificOrigins,
          builder => builder.WithOrigins("http://localhost:4200")
              .AllowAnyHeader()
              .AllowAnyMethod());
});

builder.Services.AddAuthorization(options =>
{
    options.AddPolicy(PoliciesTypes.AuthPolicy, policy =>
    {
        policy.AuthenticationSchemes.Add(JwtBearerDefaults.AuthenticationScheme);
        policy.RequireAuthenticatedUser();
        policy.Requirements.Add(new AuthRequirements(true));
    });
});

builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(options =>
{
    options.RequireHttpsMetadata = false;
    options.SaveToken = true;
    options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
    {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(tokenKey),
        ValidateIssuer = false,
        ValidateAudience = false
    };
});
// Add services to the container.

builder.Services.AddControllers();
#region custom-implementation
//Repositories
//builder.Services.AddScoped<ICabinRepository, CabinMockRepository>();
builder.Services.AddScoped<ICabinRepository, CabinRepository>();
builder.Services.AddScoped<IDiscountRepository, DiscountRepository>();

//Contexts
//builder.Services.AddDbContext<CabinContext>(opt => opt.UseInMemoryDatabase("memory"));
builder.Services.AddDbContext<CabinContext>(opt => opt.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));
//HttpClient
builder.Services.AddHttpClient();
//Services
builder.Services.AddSingleton<TokenService>();
builder.Services.AddTransient<TestApiController>();

#endregion

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "asd", Version = "v1" });
    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
    {
        Name = "Authorization",
        Type = SecuritySchemeType.ApiKey,
        Scheme = "Bearer",
        BearerFormat = "JWT",
        In = ParameterLocation.Header,
        Description = "JWT Authorization header using the Bearer scheme."
    });
    c.AddSecurityRequirement(new OpenApiSecurityRequirement { { new OpenApiSecurityScheme { Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "Bearer" } }, new string[] { } } });
});

var app = builder.Build();
app.MapGet("/", () => "Hello World!");
app.MapGet("/hello", (string name) => $"Hello {name}!");
app.MapGet("/apiversion/{api}/{version}", (string api, string version) => $"The api: {api}, v: {version}");
app.MapGet("/async", async () =>
{
    HttpClient client = new HttpClient();
    var response = await client.GetAsync("https://jsonplaceholder.typicode.com/todos/1");
    response.EnsureSuccessStatusCode();
    string responseBody = await response.Content.ReadAsStringAsync();
    return response;
});

//app.MapGet("/cabintest", async () =>
//{
//    using (var context = new CabinContext())
//    {
//        return context.Cabins.ToList();
//    }
//});

app.MapGet("/cabintestasync", async (CabinContext context) =>
{
    return await context.Cabins.ToListAsync();
});

app.MapGet("/loggerservice/data", (TokenService loggerService, [FromBody] User test) =>
{
    loggerService.Generate(test);
});

app.MapGet("/injectedrepository", (ICabinRepository cabinRepository) =>
{
    return cabinRepository.GetAll().ToList();
});

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors(MyAllowSpecificOrigins);

app.UseAuthentication();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

