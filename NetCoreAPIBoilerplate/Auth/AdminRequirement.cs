﻿using Microsoft.AspNetCore.Authorization;

namespace NetCoreAPIBoilerplate.Auth
{
    public class AdminRequirement : IAuthorizationRequirement
    {
        public int RoleLevel { get; }

        public AdminRequirement(int _RoleLevel)
        {
            RoleLevel = _RoleLevel;
        }
    }
}
