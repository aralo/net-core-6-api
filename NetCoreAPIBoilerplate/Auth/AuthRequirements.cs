﻿using Microsoft.AspNetCore.Authorization;
namespace NetCoreAPIBoilerplate.Auth
{
    public class AuthRequirements : IAuthorizationRequirement
    {
        public bool IsAuth { get; set; }

        public AuthRequirements(bool IsAuth)
        {
            this.IsAuth = IsAuth;
        }
    }
}
