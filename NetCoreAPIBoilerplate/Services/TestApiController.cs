﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace NetCoreAPIBoilerplate.Services
{
    public class TestApiController : Controller
    {

        private static readonly HttpClient client;
        private const string key = "123"; // get from appsettings.

        static TestApiController()
        {
            client = new HttpClient();
            client.DefaultRequestHeaders.Add("ApiKey", key);

        }

        public class Test
        {
            public int userId;
            public int id;
            public string title = String.Empty;
            public bool completed;
        }

        public async Task<Test?> GetDataFromAnotherEndpoint()
        {
            try
            {
                var response = await client.GetAsync("https://jsonplaceholder.typicode.com/todos/1");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                var returnedTask = JsonConvert.DeserializeObject<Test>(responseBody);
                return returnedTask;
            }
            catch (HttpRequestException ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
                return null;
            }
        }


        private const string randomNumberUrl = "https://localhost:3000/RandomNumber"; //node server.

        public async Task<string> GetRandomNumber(string Token)
        {
            using (var request = new HttpRequestMessage(HttpMethod.Get, randomNumberUrl))
            {
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                var response = await client.SendAsync(request);

                response.EnsureSuccessStatusCode();

                return await response.Content.ReadAsStringAsync();
            }
        }
    }
}
