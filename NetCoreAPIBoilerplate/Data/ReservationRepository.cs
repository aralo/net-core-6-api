﻿using NetCoreAPIBoilerplate.Context;
using NetCoreAPIBoilerplate.Models;

namespace NetCoreAPIBoilerplate.Data
{
    public class ReservationRepository : IReservationRepository
    {
        private readonly CabinContext _context;

        public ReservationRepository(CabinContext context)
        {
            _context = context;
        }
        public void AddReservation(Reservation newReservation)
        {
            if(newReservation == null)
            {
                throw new ArgumentNullException(nameof(newReservation));
            }

            _context.Reservations.Add(newReservation);
            _context.SaveChanges();
        }

        public IEnumerable<Reservation> GetAll()
        {
            return _context.Reservations.ToList();
        }

        public Reservation? GetReservationByDate(DateTime date)
        {
            return _context.Reservations.FirstOrDefault(reservation => date.Ticks > reservation.StartDate.Ticks && date.Ticks < reservation.EndDate.Ticks);
        }
    }
}
