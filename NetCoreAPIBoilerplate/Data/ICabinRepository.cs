﻿using Microsoft.AspNetCore.Mvc;
using NetCoreAPIBoilerplate.Models;

namespace NetCoreAPIBoilerplate.Data
{
    public interface ICabinRepository
    {
        IEnumerable<Cabin> GetAll();
        Task<Cabin> GetById(int id);
        Task AddCabin(Cabin newCabin);
        Task<int> DeleteCabin(int id);
    }
}
