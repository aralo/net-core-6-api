﻿using NetCoreAPIBoilerplate.Models;

namespace NetCoreAPIBoilerplate.Data
{
    public interface IDiscountRepository
    {
        Discount? GetDiscountById(int id);
        void AddDiscount(Discount discount);
    }
}
