﻿using NetCoreAPIBoilerplate.Context;
using NetCoreAPIBoilerplate.Models;

namespace NetCoreAPIBoilerplate.Data
{
    public class CabinRepository : ICabinRepository
    {
        private readonly CabinContext _context;

        public CabinRepository(CabinContext context)
        {
            _context = context;
        }

        public async Task AddCabin(Cabin newCabin)
        {
             await _context.Cabins.AddAsync(newCabin);
             await _context.SaveChangesAsync();
        }

        public async Task<int>DeleteCabin(int Id)
        {
            var cabinToDelete =  await _context.Cabins.FindAsync(Id);
            if(cabinToDelete != null)
            {
                _context.Cabins.Remove(cabinToDelete);
                return await _context.SaveChangesAsync();
            }

            throw new ArgumentNullException(nameof(cabinToDelete));
        }

        public IEnumerable<Cabin> GetAll()
        {
            return _context.Cabins.ToList();
        }

        public Cabin? GetById(int id)
        {
            return _context.Cabins.FirstOrDefault(c => c.Id == id);
        }

        Task<Cabin> ICabinRepository.GetById(int id)
        {
            throw new NotImplementedException();
        }
    }
}
