﻿using NetCoreAPIBoilerplate.Context;
using NetCoreAPIBoilerplate.Models;

namespace NetCoreAPIBoilerplate.Data
{
    public class DiscountRepository : IDiscountRepository
    {
        private readonly CabinContext _cabinContext;

        public DiscountRepository(CabinContext cabinContext)
        {
            _cabinContext = cabinContext;
        }
        public void AddDiscount(Discount discount)
        {
            if(discount == null)
            {
                throw new ArgumentNullException(nameof(discount));
            }
              _cabinContext.Discounts.Add(discount);
              _cabinContext.SaveChanges();
        }

        public Discount? GetDiscountById(int id)
        {
            return _cabinContext.Discounts.FirstOrDefault(discount => discount.Id == id);
        }

    }
}
