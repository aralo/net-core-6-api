﻿using NetCoreAPIBoilerplate.Models;

namespace NetCoreAPIBoilerplate.Data
{
    public class CabinMockRepository : ICabinRepository
    {
        List<Cabin> mockedCabinList;
        public CabinMockRepository()
        {
            this.mockedCabinList = new List<Cabin>() {
            new Cabin {  Name = "riverside", ImageUrl = "imgs/img3.png", Description = "at the side of the river" },
            new Cabin {  Name = "tree", ImageUrl = "imgs/img2.png", Description = "on a tree" }
            };
        }

        public Task AddCabin(Cabin newCabin)
        {
            throw new NotImplementedException();
        }

        //public Task<Cabin> AddCabin(Cabin newCabin)
        //{
        //    mockedCabinList.Add(newCabin);
        //    return Task.FromResult(newCabin);
        //}

        public IEnumerable<Cabin> GetAll()
        {
            return mockedCabinList;
        }

        public Task<Cabin> GetById(int id)
        {
            var findedValue = mockedCabinList.FirstOrDefault(x => x.Id == id);
#pragma warning disable CS8619 // Nullability of reference types in value doesn't match target type.
            return Task.FromResult(findedValue);
#pragma warning restore CS8619 // Nullability of reference types in value doesn't match target type.
        }

        Task<int> ICabinRepository.DeleteCabin(int id)
        {
            throw new NotImplementedException();
        }
    }
}
