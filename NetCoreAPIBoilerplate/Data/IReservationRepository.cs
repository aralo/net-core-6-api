﻿using NetCoreAPIBoilerplate.Models;

namespace NetCoreAPIBoilerplate.Data
{
    public interface IReservationRepository
    {
        void AddReservation(Reservation newReservation);
        IEnumerable<Reservation> GetAll();
        Reservation? GetReservationByDate(DateTime date);
    }
}
