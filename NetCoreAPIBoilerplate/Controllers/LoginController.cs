﻿using Microsoft.AspNetCore.Mvc;
using NetCoreAPIBoilerplate.Context;
using NetCoreAPIBoilerplate.Models;
using NetCoreAPIBoilerplate.Services;

namespace NetCoreAPIBoilerplate.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LoginController : Controller
    {
        private readonly CabinContext _cabinContext;
        private readonly TokenService _tokenGeneratorSerivce;

        public LoginController(CabinContext cabinContext, TokenService tokenGeneratorService)
        {
            _cabinContext = cabinContext;
            _tokenGeneratorSerivce = tokenGeneratorService;
        }

        [HttpPost]
        public  ActionResult<string> Login([FromBody] User credentials)
        {
            if (credentials == null)
            {
                return BadRequest();
            }
            var user = _cabinContext.Users.FirstOrDefault<User>(user => user.Username == credentials.Username && user.Password == credentials.Password );

            if(user == null)
            {
                return NotFound();
            }

            var token = _tokenGeneratorSerivce.Generate(user);

            return Ok(token);
        }

    }
}
