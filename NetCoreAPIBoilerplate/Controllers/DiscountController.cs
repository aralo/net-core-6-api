﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using NetCoreAPIBoilerplate.Data;
using NetCoreAPIBoilerplate.Models;

namespace NetCoreAPIBoilerplate.Controllers
{
    [EnableCors("CorsApi")]
    [Route("api/[controller]")]
    [ApiController]
    public class DiscountController : ControllerBase
    {
        private readonly IDiscountRepository _discountRepository;

        public DiscountController(IDiscountRepository discountRepository)
        {
            _discountRepository = discountRepository;
        }

        [HttpGet("{id}", Name = "GetDiscountById")]
        public ActionResult<Discount> GetDiscountById(int id)
        {
            var response =  _discountRepository.GetDiscountById(id);

            if (response == null)
                return NotFound();

            return Ok(response);
        }

        [HttpPost]
        public  ActionResult AddNewDiscount([FromBody] Discount newDiscount)
        {
            try
            {
                 _discountRepository.AddDiscount(newDiscount);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return CreatedAtRoute(nameof(GetDiscountById), new { Id = newDiscount.Id }, newDiscount);
        }
    }
}
