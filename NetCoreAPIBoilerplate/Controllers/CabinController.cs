﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using NetCoreAPIBoilerplate.Data;
using NetCoreAPIBoilerplate.Models;
using NetCoreAPIBoilerplate.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NetCoreAPIBoilerplate.Controllers
{
    [EnableCors("CorsApi")]
    [Route("api/[controller]")]
    [ApiController]
    public class CabinController : ControllerBase
    {
        private readonly ICabinRepository _cabinRepository;
        private readonly TestApiController _testApiController;

        public CabinController(ICabinRepository cabinRepository, TestApiController testController)
        {
            this._cabinRepository = cabinRepository;
            this._testApiController = testController;
        }

        // GET: api/<CabinController>
        [HttpGet]
        public ActionResult<IEnumerable<Cabin>> GetAll()
        {
            return Ok(_cabinRepository.GetAll());
        }

        // GET api/<CabinController>/5
        [HttpGet("{id}", Name = "GetById")]
        public async Task<ActionResult<Cabin>> GetById(int id)
        {
            var test = await _testApiController.GetDataFromAnotherEndpoint();
            try
            {
                var response = await _cabinRepository.GetById(id);

                if(response == null)
                    return NotFound();

                return Ok(response);
            }
            catch (Exception ex) 
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    $"Error: {ex}");
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult<Cabin> AddCabin([FromBody] Cabin newCabin)
        {
            try
            {
                if (newCabin == null)
                    return BadRequest();

                 _cabinRepository.AddCabin(newCabin);
                /*
                 * Returns an HTTP 201 status code if successful. HTTP 201 is the standard response for an HTTP POST method that creates a new resource on the server.
                 * Adds a Location header to the response. The Location header specifies the URI of the newly created to-do item. For more information.
                 * References the GetCabin action to create the Location header's URI. The C# nameof keyword is used to avoid hard-coding the action name in the CreatedAtAction call.          
               */
                return CreatedAtRoute(nameof(GetById), new { Id = newCabin.Id }, newCabin);

            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error creating new employee record");
            }
        }

        //// PUT api/<CabinController>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        // DELETE api/<CabinController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
               await _cabinRepository.DeleteCabin(id);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error deleting data");
            }

        }
    }
}
